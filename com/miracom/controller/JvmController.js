/**
 * Created with IntelliJ IDEA.
 * User: hyojuchoe
 * Date: 11/1/12
 * Time: 4:15 PM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('com.miracom.controller.JvmController',{
    extend:'Ext.app.Controller',
    models:['JvmLoadModel','JvmDataModel'],
    stores:['JvmLoad','JvmData'],
    views:['chart.JvmChart','panel.JvmPanel','window.JvmWindow','ViewportLayout'],
    init:function(){
        this.control({
            'button[id="startButton"]':{
                click:this.startButtonAcion
            },
            'button[name="pauseAction"]':{
                click:function(params){
                    this.pauseWindow(params);
                }
            },
            'menuitem[name="winObjItem"]':{
                click:function(params){
                    this.focusMe(params);
                }
            },
            'menuitem[name="controllSpeedAction"]':{
                click:function(params){
                    var windowCmp = params.handler();
                    if(params.text == 'slow')
                        this.controlSpeed(windowCmp,'slow');
                    else
                        this.controlSpeed(windowCmp,'fast');
                }

            },
            'menuitem[name="lineConAction"]':{
                click:function(items){
                    this.checkButtonProcess(items);
                }
            },
            'window':{
                minimize:function(window){
                    window.setVisible(false);
                },
                destroy:function(window){
                    this.removeToolbarItem(window);
                    this.destroyDataStore(window);
                }
            }
        });
    },

    startButtonAcion:function(){
        this.makeWindow();
    },

    makeWindow:function(){
        var jvmLoadStore = Ext.create('com.miracom.store.JvmLoad');
        var jvmDataStore = Ext.create('com.miracom.store.JvmData');
        var windowComponent = Ext.create('com.miracom.view.window.JvmWindow',{
            store:jvmDataStore,jvmLoadStore:jvmLoadStore
        }).show();
        this.addToolbarItem(windowComponent.id);
        this.updateWindows(windowComponent);
    },

    addToolbarItem:function(windowID){
        var viewportLayout = Ext.getCmp('viewportLayout');
        var objectID = viewportLayout.objectID;
        var toolbar = Ext.getCmp('showWindowMenu');
        var objButton = Ext.create('Ext.Action',{
            name:'winObjItem',text:windowID,handler:function(){return this.text;}});
        toolbar.menu.add(objButton);
        objectID.push({id:windowID,objbtn:objButton.itemId});
    },

    removeToolbarItem:function(windowCmp){
        var objectIDArray = Ext.getCmp('viewportLayout').objectID;
        var toolbar = Ext.getCmp('showWindowMenu');

        for(var obj=0; obj < objectIDArray.length;obj++){
            if(objectIDArray[obj].id == windowCmp.id){
                toolbar.menu.remove(objectIDArray[obj].objbtn);
                objectIDArray.splice(objectIDArray.indexOf(windowCmp.id,1));
            }
        }
    },

    updateWindows: function(windowCmp){
        var chartCmp = Ext.getCmp(windowCmp.getChartID());

        windowCmp.loadInterval = setInterval(updateChart, windowCmp.updateChartInterval);

        function updateChart(){
            windowCmp.jvmLoadStore.load({
                callback:function(records){
                    checkStore();
                    setAndPutDate(records,windowCmp.store);
                    shiftAxis();
                }
            });
        }

        function checkStore(){
            if(!windowCmp.storeCheck){
                windowCmp.store.removeAll(true);
                windowCmp.storeCheck = true;
            }
        }

        function shiftAxis(){
            var timeAxis = chartCmp.axes.get(1);
            var toDate = timeAxis.toDate;
            if (+toDate < +windowCmp.currentDate) {
                timeAxis.toDate = windowCmp.currentDate;
                timeAxis.fromDate = Ext.Date.add(Ext.Date.clone(timeAxis.toDate), Ext.Date.SECOND, -windowCmp.timeAixsWidth);
                windowCmp.store.removeAt(0);
            }
        }

        function setAndPutDate(records,dataStore){
            var setCpuValue = records[0].get('cpu');
            var setMemValue = records[0].get('memory');
            var currentDate = new Date(records[0].get('date'));
            windowCmp.currentDate = currentDate;
            dataStore.add({cpu:setCpuValue,memory:setMemValue,date:currentDate});
        }
    },

    pauseWindow: function(params){
        var windowCmp = params.handler();
        var dockItems = windowCmp.getDockedItems()[1].items.get(4);
        var chartCmp = Ext.getCmp(windowCmp.getChartID());

        if(windowCmp.pauseIntervalFlag){
            buttonChange('CONTINUE',false);
            chartCmp.getEl().mask();
            clearTimeout(windowCmp.loadInterval);
            windowCmp.pauseInterval = setInterval(saveDataWithinPause,windowCmp.updateChartInterval);
        }
        else{
            buttonChange('PAUSE',true);
            chartCmp.getEl().unmask();
            clearTimeout(windowCmp.pauseInterval);
            loadData(windowCmp.pauseStore);
            this.updateWindows(params.handler());
        }

        function buttonChange(text,flag){
            params.setText(text);
            dockItems.setDisabled(!flag);
            windowCmp.pauseIntervalFlag = flag;
        }

        function saveDataWithinPause(){
            windowCmp.jvmLoadStore.load({
                callback:function(records){
                    setAndPutDate(records,windowCmp.pauseStore);
                    checkPauseStore(windowCmp.pauseStore);
                }
            })
        }

        function loadData(storeData){
            if(windowCmp.store.count() >=windowCmp.timeAixsWidth){
                if(chartCmp.axes.get(1).fromDate >= windowCmp.store.getAt(0).get('date')){
                    windowCmp.store.removeAt(0,storeData.length);
            }}
            windowCmp.store.add(storeData);
            windowCmp.pauseStore=[];
        }

        function checkPauseStore(pauseStore){
            if(pauseStore.length>windowCmp.timeAixsWidth)
                pauseStore.shift();
        }

        function setAndPutDate(records,dataStore){
            var setCpuValue = records[0].get('cpu');
            var setMemValue = records[0].get('memory');
            var currentDate = new Date(records[0].get('date'));
            windowCmp.currentDate = currentDate;
            dataStore.push({cpu:setCpuValue,memory:setMemValue,date:currentDate});
        }
    },

    controlSpeed : function(windowCmp,option){
        var givenInterval = (option == 'slow' ? 1000 : 500);
        windowCmp.updateChartInterval = givenInterval;
        clearTimeout(windowCmp.loadInterval);
        this.updateWindows(windowCmp);
    },

    checkButtonProcess : function(item){
        var windowCmp = Ext.getCmp(item.findParentByType('window').getId());
        var chartCmp = Ext.getCmp(windowCmp.getChartID());

        if(item.checked)
            this.showItem(item,chartCmp);
        else
            this.hideItem(item,chartCmp);
    },

    hideItem: function(item,chartCmp){
        if(item.text == 'cpu')
            chartCmp.series.get(1).hideAll();
        else
            chartCmp.series.get(0).hideAll();
    },

    showItem: function(item,chartCmp){
        if(item.text == 'cpu')
            chartCmp.series.get(1).showAll();
        else
            chartCmp.series.get(0).showAll();
    },

    focusMe: function(params){
        var windowCmp = Ext.getCmp(params.handler());
        if(!windowCmp.isVisible())
            windowCmp.setVisible(true);
        Ext.WindowManager.bringToFront(windowCmp.id);
    },

    destroyDataStore: function(windowCmp){
        windowCmp.store.destroy();
    }
});