/**
 * Created with IntelliJ IDEA.
 * User: hyojuchoe
 * Date: 11/5/12
 * Time: 9:21 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('com.miracom.model.JvmDataModel',{
    extend:'Ext.data.Model',
    fields:[{name:'cpu',type:'int'},{name:'memory',type:'int'},{name:'date',type:'date'}],
    proxy:{type:'localstorage',id:'cpudata-store'}
});