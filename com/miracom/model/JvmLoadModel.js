/**
 * Created with IntelliJ IDEA.
 * User: hyojuchoe
 * Date: 11/1/12
 * Time: 4:15 PM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('com.miracom.model.JvmLoadModel',{
    extend:'Ext.data.Model',
    fields:[{name:'cpu',type:'int'},{name:'memory',type:'int'},{name:'date',type:'long'}],
    proxy:{type:'rest',url:'resources/jvmload'}
});