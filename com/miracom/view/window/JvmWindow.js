/**
 * Created with IntelliJ IDEA.
 * User: hyojuchoe
 * Date: 11/8/12
 * Time: 1:49 PM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('com.miracom.view.window.JvmWindow',{
    extend:'Ext.window.Window',
    title:'CPU USES',
    alias:'widget.jvmwindow',
    width:800,
    height:450,
    minWidth:150,
    minHeight:100,
    layout:'fit',
    minimizable:true,
    initComponent:function(){
        var me = this;
        var windowPauseButton = {
            name:'pauseAction',
            text: 'PAUSE',
            enableToggle:true,
            handler:function(){return me;}
        };

        var slowButton = {
            name:'controllSpeedAction',
            text:'slow',
            checked:false,
            group:'speed',
            handler:function(){return me;}
        };

        var fastButton = {
            name:'controllSpeedAction',
            text:'fast',
            checked:true,
            group:'speed',
            handler:function(){return me;}
        };
        var menuButton = {text:'Speed',menu:[slowButton,fastButton]};

        var showLineButton = {text:'show',menu:[
            {name:'lineConAction',text:'cpu',checked:true,checkHandler:this.markingButton},
            {name:'lineConAction',text:'mem',checked:true,checkHandler:this.markingButton}
        ]};

        this.items=[{xtype:'jvmchart',store:this.store,timeAixsWidth:this.timeAixsWidth}];
        this.dockedItems={xtype:'toolbar',items:[windowPauseButton,'->',showLineButton,'-',menuButton]};
        this.callParent(arguments);
    },
    loadInterval:{},
    pauseInterval:{},
    jvmLoadStore:{},
    currentDate:{},
    pauseStore:[],
    timeAixsWidth:100,
    updateChartInterval:500,
    pauseIntervalFlag:true,
    storeCheck:false,

    markingButton:function(item){
        return item;
    },

    getChartID: function(){
        return this.items.get(0).getId();
    }
});