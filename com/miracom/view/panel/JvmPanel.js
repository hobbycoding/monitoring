/**
 * Created with IntelliJ IDEA.
 * User: hyojuchoe
 * Date: 11/5/12
 * Time: 8:46 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('com.miracom.view.panel.JvmPanel',{
    extend:'Ext.panel.Panel',
    title:'CPU USES',
    alias:'widget.jvmpanel',
    width:800,
    height:450,
    minWidth:150,
    minHeight:100,
    layout:'fit',
    initComponent:function(){
        this.id ='jvmpanelcmp';
        this.items=[{xtype:'jvmchart'}];
        panelPauseButton = Ext.create('Ext.Action', {
            name:'pauseAction',
            text: 'PAUSE'
        });
        this.dockedItems={xtype:'toolbar',items:[panelPauseButton]};
        this.callParent(arguments);
    },

    setButtonText: function (text){
        panelPauseButton.setText(text);
    }
});