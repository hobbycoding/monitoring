/**
 * Created with IntelliJ IDEA.
 * User: hyojuchoe
 * Date: 11/1/12
 * Time: 4:16 PM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('com.miracom.view.chart.JvmChart',{
    extend:'Ext.chart.Chart',
    alias:'widget.jvmchart',
    width:800,
    height:450,
    minWidth:150,
    minHeight:100,
    animate:false,
    style:'background:#fff',
    initComponent:function(){
        this.axes = [{
            type: 'Numeric',
            position: 'left',
            fields: ['cpu','memory'],
            title: 'Usage %',
            grid: true,
            decimals: 0,
            minimum: 0,
            maximum: 100
        }, {
            type: 'Time',
            position: 'bottom',
            fields: 'date',
            dateFormat: 'H:i:s',
            step:[Ext.Date.MINUTE,1],
            fromDate:new Date(),
            toDate: Ext.Date.add(new Date(),Ext.Date.SECOND,this.timeAixsWidth),
            constrain: true,
            grid: true
        }];
        this.series = [{
            type:'area',
            axis:'left',
            highlight:true,
            xField:'date',
            yField:['memory'],
            style: {lineWidth: 1,stroke: '#666',opacity: 0.86},
            tips:{trackMouse: true,width: 70,height: 30,
                renderer: function(storeItem, item) {
                    var setData = storeItem.get('memory');
                    this.setTitle('memory : ' + +setData+'%');
                }
            }
            },{
            type:'area',
            axis:'left',
            highlight:true,
            xField:'date',
            yField:['cpu'],
            style: {lineWidth: 1,stroke: 'rgb(213, 70, 121)',opacity: 0.86},
            tips:{trackMouse: true,width: 70,height: 30,
                renderer: function(storeItem, item) {
                    var setData = storeItem.get('cpu');
                    this.setTitle('cpu : ' + +setData+'%');
                }
            }}];
        this.callParent(arguments);
    },
    timeAixsWidth:{}
});