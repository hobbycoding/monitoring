/**
 * Created with IntelliJ IDEA.
 * User: hyojuchoe
 * Date: 11/12/12
 * Time: 10:14 AM
 * To change this template use File | Settings | File Templates.
 */
Ext.define('com.miracom.view.ViewportLayout',{
    extend:'Ext.container.Viewport',
    layout:'border',
    bodyPadding:5,
    initComponent:function(){
        this.id='viewportLayout';
        var northToolbarItem = {
            xtype: 'toolbar',
                dock: 'top',
                id: 'statusToolbar',
                items: [
                {
                    xtype: 'button',
                    id: 'startButton',
                    text: 'START'
                },
                '->',
                {
                    id:'showWindowMenu',
                    menu:[]
                }
            ]
        };
        this.items=[
            {region:'north',height:50,title:'Server JVM Status',dockedItems:northToolbarItem},
            {region:'center'}
        ];
        this.callParent(arguments);
    },
    objectID:[]
});