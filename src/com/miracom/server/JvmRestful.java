package com.miracom.server;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.Math;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.Path;

/*
 * Created with IntelliJ IDEA.
 * User: hyojuchoe
 * Date: 10/5/12
 * Time: 11:08 AM
 * To change this template use File | Settings | File Templates.
 */
@Path("/jvmload")
public class JvmRestful {

    private static Runtime jvmruntime;
    private static RuntimeMXBean jvmBean;
    private static com.sun.management.OperatingSystemMXBean sunOSMBean;

    private static long prevUpTime, prevProcessCpuTime;
    private static long upTime,processCpuTime;
    private static double cpuUsage;
    private static int cpuCount;

    @GET
    @Produces("application/json")
    public Map sendJvmData() {
        Map<String,Object> v = new HashMap<String, Object>();
        try{
            v.put("cpu",getCPULoad());
            v.put("memory",getFreeMemory());
            v.put("date",getCurrentDate());
        }catch (Exception e){System.out.print("!");}
        return v;
    }

    static {
        try{
            jvmruntime = Runtime.getRuntime();
            jvmBean = ManagementFactory.getRuntimeMXBean();
            sunOSMBean  = ManagementFactory.newPlatformMXBeanProxy(
                    ManagementFactory.getPlatformMBeanServer(),
                    ManagementFactory.OPERATING_SYSTEM_MXBEAN_NAME,
                    com.sun.management.OperatingSystemMXBean.class
            );
            cpuCount = sunOSMBean.getAvailableProcessors();
        }catch (Exception e){System.out.print("!");}
    }

    public int getCPULoad(){

        upTime = jvmBean.getUptime();
        processCpuTime = sunOSMBean.getProcessCpuTime();

        if(upTime > 0L && processCpuTime >= 0L)
            updateCPUInfo();
        return (int)cpuUsage;

    }

    public int getFreeMemory(){
        float totalmemory = jvmruntime.totalMemory();
        float freememory = jvmruntime.freeMemory();
        float v =(freememory/totalmemory)*100;
        return (int)v;
    }

    public void updateCPUInfo() {
        if (prevUpTime > 0L && upTime > prevUpTime) {
            float elapsedCpu =processCpuTime - prevProcessCpuTime;
            float elapsedTime = upTime - prevUpTime;
            float calculateTime = (elapsedCpu / (elapsedTime * 10000F * cpuCount));
            cpuUsage = Math.round(Math.min(100F,calculateTime));
        }
        prevUpTime = upTime;
        prevProcessCpuTime = processCpuTime;
    }

    public Long getCurrentDate(){
        Date date = new Date();
        return date.getTime();
    }
}
